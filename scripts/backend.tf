terraform {
    backend "azurerm" {
        resource_group_name  = "phakaratterraform"
        storage_account_name = "phakaratterraform"
        container_name       = "phakaratterraform"
        key                  = "phakaratterraform.tfstate"
    }
}